# slack-mini-client

A simple Slack client for bots with the following features:

 - Mappable config class with a full range of config fields
 - Attaches itself to a single channel per instance.
 - Authenticates as bot user.
 - Can send messages with attachments and fields.
 - Can post files with a comment.
 
## Maven Depenendencies
 
We use jitpack.io for [release listings, distribution and Maven documentation](https://jitpack.io/#org.bitbucket.agilestationery/slack-mini-client)

## Who we are

This project is managed by Simon of [Agile Stationery](https://agilestationery.co.uk)