package uk.co.agilestationery.slack;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Simon
 * @since 08/09/2017
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SlackResponse {

	private boolean ok;
	private String error;

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
