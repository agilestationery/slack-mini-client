package uk.co.agilestationery.slack;

/**
 * @author Simon
 * @since 13/09/2017
 */
public class Field {

	String title;
	String value;
	boolean isShort;

	public Field(String title, String value, boolean isShort) {
		this.title = title;
		this.value = value;
		this.isShort = isShort;
	}

	public String getTitle() {
		return title;
	}

	public String getValue() {
		return value;
	}

	public boolean isShort() {
		return isShort;
	}

}
