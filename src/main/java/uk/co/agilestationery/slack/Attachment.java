package uk.co.agilestationery.slack;

import java.util.List;

/**
 * @author Simon
 * @since 15/09/2017
 */
public class Attachment {

	private String text;
	private List<Field> fields;
	private long ts;
	private String footer;

	public Attachment(String text, List<Field> fields, long ts, String footer) {
		this.text = text;
		this.fields = fields;
		this.ts = ts;
		this.footer = footer;
	}

	public String getText() {
		return text;
	}

	public List<Field> getFields() {
		return fields;
	}

	public long getTs() {
		return ts;
	}

	public String getFooter() {
		return footer;
	}
}
