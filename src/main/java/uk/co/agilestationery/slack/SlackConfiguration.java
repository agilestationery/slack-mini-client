package uk.co.agilestationery.slack;


/**
 * SlackConfiguration
 *
 * @author Simon
 */
public class SlackConfiguration {

	private String clientId;
	private String clientSecret;
	private String verification;
	private String oauthToken;
	private String botOauthToken;
	private String channelId;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getVerification() {
		return verification;
	}

	public void setVerification(String verification) {
		this.verification = verification;
	}

	public String getOauthToken() {
		return oauthToken;
	}

	public void setOauthToken(String oauthToken) {
		this.oauthToken = oauthToken;
	}

	public String getBotOauthToken() {
		return botOauthToken;
	}

	public void setBotOauthToken(String botOauthToken) {
		this.botOauthToken = botOauthToken;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
}
