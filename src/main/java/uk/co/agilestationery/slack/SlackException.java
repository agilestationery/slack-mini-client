package uk.co.agilestationery.slack;

/**
 * @author Simon
 * @since 08/09/2017
 */
public class SlackException extends RuntimeException {
	public SlackException(String message) {
		super(message);
	}

	public SlackException(Exception cause) {
		super(cause);
	}
}
