package uk.co.agilestationery.slack;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;



import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * @author Simon
 * @since 08/09/2017
 */
public class SlackClient {

	private static final String FILE_UPLOAD_METHOD_URL = "/api/files.upload";

	public static final HttpHost SLACK_API_HOST = new HttpHost("slack.com", -1, "https");


	private SlackConfiguration configuration;

	public SlackClient(SlackConfiguration configuration) {
		this.configuration = configuration;
	}

	public void postFile(byte[] data, String filename, String comment, ContentType mediaType) {

		checkNotNull(data, "data");
		checkNotNull(filename,"filename");
		checkNotNull(comment, "comment");

		try(CloseableHttpClient client = findClient();) {

			HttpEntity entity = MultipartEntityBuilder.create()
					.addTextBody("token", configuration.getBotOauthToken())
					.addTextBody("channels", configuration.getChannelId())
					.addTextBody("filename", filename)
					.addTextBody("initial_comment", comment)
					.addBinaryBody("file", data, mediaType, filename)
					.build();

			HttpPost httpPost = new HttpPost(FILE_UPLOAD_METHOD_URL);
			httpPost.setEntity(entity);

			postToSlack(client, httpPost);

		} catch (IOException ioe) {
			throw new SlackException(ioe);
		}

	}

	public void postMessage(String message, Attachment... attachments) {
		try(CloseableHttpClient client = findClient();) {

			List<NameValuePair> parameters = new ArrayList<>(4);

			parameters.add(new BasicNameValuePair("token", configuration.getBotOauthToken()));
			parameters.add(new BasicNameValuePair("channel", configuration.getChannelId()));
			parameters.add(new BasicNameValuePair("text", message));
			parameters.add(new BasicNameValuePair("attachments", toJson(attachments)));
			parameters.add(new BasicNameValuePair("as_user","true"));

			EntityBuilder entityBuilder = EntityBuilder.create().setParameters(parameters);

			HttpEntity entity = entityBuilder.build();

			HttpPost httpPost = new HttpPost("https://slack.com/api/chat.postMessage");
			httpPost.setEntity(entity);

			postToSlack(client, httpPost);

		} catch (IOException ioe) {
			throw new SlackException(ioe);
		}
	}

	private String toJson(Attachment[] attachments) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter writer = mapper.writerFor(Attachment[].class);

		try {
			return writer.writeValueAsString(attachments);
		} catch (JsonProcessingException e) {
			throw new SlackException(e);
		}

	}

	private void postToSlack(CloseableHttpClient client, HttpPost httpPost) throws IOException {
		HttpResponse response = client.execute(SLACK_API_HOST, httpPost);

		try(InputStream entityStream = response.getEntity().getContent()) {
			ObjectMapper mapper = new ObjectMapper();
			SlackResponse slackResponse = mapper.readerFor(SlackResponse.class).readValue(entityStream);

			if (!slackResponse.isOk()) {
				throw new SlackException("Upload failed: " + slackResponse.getError());
			}
		}
	}

	private CloseableHttpClient findClient() {
		return HttpClientBuilder.create().build();
	}
}
